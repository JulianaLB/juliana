import React from "react";
import { Switch, Route } from "react-router-dom";

import Dashboard from "../pages/Dashboard";
import PostDetail from "../pages/PostDetail";

export default function Routes() {
  return (
    <Switch>
      <Route path="/" component={props => <Dashboard {...props} />} />
      <Route path="/details" component={props => <PostDetail {...props} />} />
    </Switch>
  );
}
