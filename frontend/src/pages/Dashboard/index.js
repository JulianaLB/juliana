import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Navbar,
  Container,
  Card,
  Badge,
  ButtonGroup,
  Button,
  ButtonToolbar,
  Modal,
  Form
} from "react-bootstrap";

import { loadPosts, loadCategories } from "../../services";
import "./styles.css";
import Eye from "../../assets/eye.png";
import history from "../../routes/history";
import PostModal from "../../components/PostModal";

const Dashboard = () => {
  const dispatch = useDispatch();
  const [postsList, setPostsList] = useState([]);
  const [categories, setCategories] = useState([]);

  const [filteredPosts, setFilteredPosts] = useState([]);
  const [selectedCat, setSelectedCat] = useState();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    loadPosts(setPostsList, setFilteredPosts);
    loadCategories(dispatch, setCategories);
  }, []);

  useEffect(() => {
    const filter = postsList.filter(p => p.category === selectedCat);
    setFilteredPosts(filter);
  }, [selectedCat]);

  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src={Eye}
            width="35"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          Readdit
        </Navbar.Brand>
      </Navbar>
      <Navbar>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>Posts: {postsList.length}</Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <Container>
        <ButtonToolbar className="justify-content-between">
          <ButtonGroup size="lg" className="mb-2">
            {categories.map(c => (
              <Button onClick={() => setSelectedCat(c.name)}>
                {c.name.charAt(0).toUpperCase() + c.name.slice(1)}
              </Button>
            ))}
            <Button onClick={() => setFilteredPosts(postsList)}>
              Clean Filter
            </Button>
          </ButtonGroup>
          <Button variant="success" onClick={handleShow}>
            Add Post
          </Button>
        </ButtonToolbar>
        {filteredPosts.map(p => (
          <Card className="col-md-12">
            <Card.Body className="col-md-12">
              <Card.Title>{p.title}</Card.Title>
              <Card.Subtitle>
                Created by {p.author} - {p.timestamp}
              </Card.Subtitle>
              <div className="row">
                <div className="col-md-4">
                  <Badge variant="info">{p.category}</Badge>
                </div>
                <div className="col-md-4">
                  <small className="text-muted">Votes: {p.voteScore}</small>
                </div>
                <div className="col-md-4">
                  <small className="text-muted">
                    Comments: {p.commentCount}
                  </small>
                </div>
              </div>
            </Card.Body>
            <Card.Footer className="col-md-12">
              <Button onClick={() => history.push("/details", { post: p })}>
                View Post
              </Button>
              <Card.Link href="#">Edit Post</Card.Link>
              <Card.Link href="#">Delete Post</Card.Link>
            </Card.Footer>
          </Card>
        ))}
      </Container>
      <PostModal
        type={"Add"}
        show={show}
        handleClose={handleClose}
        categories={categories}
      />
    </>
  );
};

export default Dashboard;
