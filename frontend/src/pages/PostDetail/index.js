import React from "react";
import {
  Navbar,
  Container,
  Row,
  Card,
  Button,
  ListGroup
} from "react-bootstrap";

import Eye from "../../assets/eye.png";

class PostDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = { post: props.location.state.post, comments: [] };
  }
  static getDerivedStateFromProps(props, state) {
    fetch(
      `http://localhost:3001/posts/${props.location.state.post.id}/comments`,
      {
        headers: { Authorization: "auth-juliana" }
      }
    )
      .then(response => response.json())
      .then(comments => {
        state.comments = comments;
      });
  }
  render() {
    console.log(this.state.comments);
    return (
      <>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">
            <img
              alt=""
              src={Eye}
              width="35"
              height="30"
              className="d-inline-block align-top"
            />{" "}
            Readdit
          </Navbar.Brand>
        </Navbar>
        <Navbar bg="light" variant="light">
          <Navbar.Brand href="./Dashboard">Voltar</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>Comments: 2</Navbar.Text>
          </Navbar.Collapse>
        </Navbar>
        <Container>
          <Row>
            <Card className="col-md-3">
              <ListGroup variant="flush">
                <ListGroup.Item>
                  Created by {this.state.post.author}
                </ListGroup.Item>
                <ListGroup.Item>{this.state.post.timestamp}</ListGroup.Item>
                <ListGroup.Item>
                  Category: {this.state.post.category}
                </ListGroup.Item>
              </ListGroup>
              <Card.Body>
                <Button variant="primary" className="col-md-5">
                  Edit Post
                </Button>
                <Button variant="danger" className="col-md-6 offset-1">
                  Delete Post
                </Button>
              </Card.Body>
            </Card>
            <Card className="col-md-8 offset-1">
              <Card.Header as="h3">{this.state.post.title}</Card.Header>
              <Card.Body>
                <Card.Text>{this.state.post.body}</Card.Text>
              </Card.Body>
            </Card>
          </Row>
          <Row>
            <h4 className="col-md-3">Comments:</h4>
          </Row>
          <Row>
            <Card className="col-md-12">
              <Card.Body>
                <Card.Title>Hi there! I am a COMMENT.</Card.Title>
                <Card.Subtitle>
                  <small className="text-muted">
                    Created by thingtwo - 12/07/2020
                  </small>
                </Card.Subtitle>
              </Card.Body>
              <Card.Footer>
                <Card.Link href="#">Edit Post</Card.Link>
                <Card.Link href="#">Delete Post</Card.Link>
              </Card.Footer>
            </Card>
          </Row>
          <Row>
            <Card className="col-md-12">
              <Card.Body>
                <Card.Title>Comments. Are. Cool.</Card.Title>
                <Card.Subtitle>
                  <small className="text-muted">
                    Created by thingone - 20/09/2020
                  </small>
                </Card.Subtitle>
              </Card.Body>
              <Card.Footer>
                <Card.Link href="#">Edit Post</Card.Link>
                <Card.Link href="#">Delete Post</Card.Link>
              </Card.Footer>
            </Card>
          </Row>
        </Container>
      </>
    );
  }
}

export default PostDetail;
