import React from "react";
import { Router } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import history from "./routes/history";
import Routes from "./routes/index";

function App() {
  return (
    <div>
      <Router history={history} forceRefresh={true}>
        <Routes />
      </Router>
    </div>
  );
}

export default App;
