import { addCategories } from "./store/actions";

const loadPosts = (setPostsList, setFilteredPosts) => {
  fetch("http://localhost:3001/posts", {
    headers: { Authorization: "auth-juliana" }
  })
    .then(response => response.json())
    .then(posts => {
      setPostsList(posts);
      setFilteredPosts(posts);
    });
};

const addPost = detail => {
  fetch("http://localhost:3001/posts", {
    headers: { Authorization: "auth-juliana" },
    method: "POST",
    body: JSON.stringify(detail)
  })
    .then(response => response.json())
    .then(posts => {
      console.log(posts);
    });
};

const loadCategories = (dispatch, setCategories) => {
  fetch("http://localhost:3001/categories", {
    headers: { Authorization: "auth-juliana" }
  })
    .then(response => response.json())
    .then(categories => {
      setCategories(categories.categories);
      dispatch(addCategories(categories.categories));
    });
};

export { loadPosts, addPost, loadCategories };
