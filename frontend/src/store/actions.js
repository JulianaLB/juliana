export function addPost(payload) {
  return { type: "ADD_POST", payload };
}

export function updatePosts(payload) {
  return { type: "UPDATE_POSTS", payload };
}

export function addComment(payload) {
  return { type: "ADD_COMMENT", payload };
}

export function updateComments(payload) {
  return { type: "UPDATE_COMMENTS", payload };
}

export function addCategories(payload) {
  return { type: "ADD_CATEGORIES", payload };
}
