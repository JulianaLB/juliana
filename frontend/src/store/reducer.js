const initialState = {
  posts: [],
  categories: [],
  comments: []
};

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case "ADD_POST":
      state.posts.push(action.payload);
      break;
    case "UPDATE_POSTS":
      state.posts = action.payload;
      break;
    case "ADD_CATEGORIES":
      state.categories = action.payload;
      break;
    case "ADD_COMMENT":
      state.comments.push(action.payload);
      break;
    case "UPDATE_COMMENTS":
      state.comments = action.payload;
      break;
    default:
  }
}

export default rootReducer;
