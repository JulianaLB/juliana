import React, { useState } from "react";
import { Button, Modal, Form } from "react-bootstrap";

import { addPost } from "../../services";

const PostModal = ({ type, show, handleClose, categories }) => {
  const [validated, setValidated] = useState(false);
  const handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidated(true);
    // Check the type to determine if it's create or update that needs to be called

    if (type === "Add") {
      // TO DO - make it dynamic
      const body = {
        id: "466486548zsrfdb63gh4dz6f35h47",
        timestamp: Date.now(),
        title: "title",
        body: "body",
        author: "Juliana",
        category: "react"
      };
      addPost(body);
    }
  };
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{type} Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group controlId="formTitle">
              <Form.Label>Title</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Title of the post"
              />
            </Form.Group>
            <Form.Group controlId="formCategory">
              <Form.Label>Category</Form.Label>
              <Form.Control required as="select">
                {categories.map(c => (
                  <option>{c.name}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="formBody">
              <Form.Label>Body</Form.Label>
              <Form.Control required as="textarea" rows={3} />
            </Form.Group>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button type="submit">Save Changes</Button>
          </Form>
        </Modal.Body>
        {/* <Modal.Footer>

        </Modal.Footer> */}
      </Modal>
    </>
  );
};

export default PostModal;
