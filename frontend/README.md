# Test Application - Juliana Boudakian

This project can be executed with the following steps:

### `cd frontend`

### `npm install`

### `npm start`

## `My TO DO list:`

- Create Posts screen (static) _OK_
- Create Post Details screen (static) _OK_
- Create Redux in the project _OK_
- Call API to list posts dynamically _OK_
- Create "category" filter _OK_
- Create Routes _OK_
- "View Post" button redirect to Post Details screen _PARTIAL: history.push doesn't refresh component, find a way to refresh it. Manual refresh works for now_
- Pass post through props to details screen _OK_
- Call API to show comments dynamically _PARTIAL: fetch() created but it still needs refactoring to set props correctly_
- Create button "Add Post" _OK_
- Create Modal do add post (OBS: Make modal receive a type "add" or "edit". So that is possible to reuse as the edit Modal) _OK_
- Call API to add post \_PARTIAL: get information from form and pass it to the function in services.js
- "Delete Post" button call API to delete post
- Create Modal to edit post (OBS: modal already created, refactor to fill the fields and update instead of add)
- Create Modal to edit comment
- "Delete Comment" button call API to delete comment
- Refactor vote part on the cards to put an arrow on each side with the amount of votes in the middle (On posts and comments)
- Create functionality to add/remove votes (On posts and comments)
- Order comments and posts by amount of votes
- NICE TO HAVE: put all of API calls in a separate file. _CREATED_

## `Tests to do:`

- Test responsive with different sizes of screen
- Add/Remove/Edit posts/comments and see if the screens reflect
- Increase and decrease votes to check order.
